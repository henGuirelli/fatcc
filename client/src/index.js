import React from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
// Config
import { routers } from './config.json'

// Css
import './index.css'
import './styles/config.css'

// Pages
import Login from './pages/Login'
import Cadastro from './pages/Cadastro'
import Perfil from './pages/Perfil'
import MeuGrupoOpcoes from './pages/MeuGrupoOpcoes'
import CriarGrupo from './pages/CriarGrupo'
import BuscarGrupo from './pages/BuscarGrupo'
import MeuGrupo from './pages/MeuGrupo'
import Chat from './pages/Chat'
import Home from './pages/Home'
import Orientador from './pages/Orientador'

/* material ui */
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import themeConfig from './styles/theme.json'


ReactDOM.render(
    <BrowserRouter>
        <Switch>
                <Route path={routers.home} exact={true} component={Home} />
                <Route path={routers.login} component={Login} />
                <Route path={routers.cadastro} exact={true} component={Cadastro} />
                <Route path={routers.perfil} exact={true} component={Perfil} />
                <Route path={routers.opcoesGrupo} exact={true} component={MeuGrupoOpcoes} />
                <Route path={routers.criarGrupo} exact={true} component={CriarGrupo} />
                <Route path={routers.buscarGrupo} exact={true} component={BuscarGrupo} />
                <Route path={routers.grupo} exact={true} component={MeuGrupo} />
                <Route path={routers.chat} exact={true} component={Chat} />
                <Route exact path={routers.orientador} component={Orientador} />
        </Switch>
    </ BrowserRouter>
    , document.getElementById('root'))
registerServiceWorker()
