import React, { Fragment } from 'react'
import './style.css'
import Button from '../Button';
import config from '../../config.json'
import swal from 'sweetalert'

const CardEspecialidades = ({ orientador, close, cardIndex }) => {
    console.log(orientador)
    return    <div className='container-card-orientador'>
        <div className={'card-orientador '} >
            <h2>{ orientador.nome }</h2>
            <p> {orientador.especialidades} </p>
            <Button text='Solicitar' onClick={() => solicitarOrientacao(orientador.id)}/>
            <Button text='Fechar' onClick={close}/>
            <br/>
        </div>
    </div>
}

const solicitarOrientacao = (idOrientador) => {
    let idGrupo = JSON.parse(sessionStorage.getItem('user')).id_grupo
    console.log(idOrientador)
    console.log(idGrupo)
    fetch (config.endpoints.solicitarOrientacao.replace(':id_usuario', idOrientador), {
        method: 'put',
        body: JSON.stringify(idGrupo)
    })
    .then(result => result.json())
    .then(json => {
        if (json.success){
            swal('sucesso')
        }else{
            swal('falha ao solicitar')
        }
    })
}

class Table  extends React.Component {
    state = {cardVisible: false, cardIndex: 0}

    toggleVisible = (index) => {
        //console.log(index)
        this.setState({cardVisible: !this.state.cardVisible, cardIndex: index || 0})
    }

    render(){
        const { orientadores } = this.props
        const { cardIndex, cardVisible } = this.state
        return (
            <Fragment>
                { cardVisible ? <CardEspecialidades orientador={orientadores[cardIndex]} cardIndex={cardIndex} close={this.toggleVisible} /> : null }
                <table className='table-orientador'>
                    <thead className='head'>
                        <tr>
                            <th>Orientador</th>
                        </tr>
                    </thead>
                    <tbody className='conteudo'>
                        { orientadores.map((orientador, index) => <tr onClick={() => this.toggleVisible(index)}><td> { orientador.nome } </td></tr>) }
                    </tbody>
                </table>
            </Fragment>
        )
    }
}

export default Table