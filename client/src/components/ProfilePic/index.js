import React from 'react'
import './style.css'
import defaultProfilePic from '../../images/profile/default-user-profile.jpg'

const ProfilePic = ({src}) => {
    return (
        <img className="profile-pic" src={src || defaultProfilePic} alt="imagem de perfil"/>
    )
}

export default ProfilePic