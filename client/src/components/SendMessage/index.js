import React from 'react'
import { withFormik, Form, Field } from 'formik'
import SendIMG from '../../images/sys_images/send.svg'
import swal from 'sweetalert'
import './style.css'

import config from '../../config.json'

const SendMessage = () => {
    return (
        <Form>
            <div className='enviar-mensagem'>
                <div className='inline texto'>
                    <Field name='text' type='text' placeholder='Digite aqui...' className='text-area' autoFocus />
                </div>
                <div className='inline enviar'>
                    <Field name='enviar' type='submit' />
                </div>
            </div>
        </Form>
    )
}

const FormikApp = withFormik({
    handleSubmit(values){
        let user = JSON.parse(sessionStorage.getItem('user'))
        let sender = {}
        sender.mensagem = values.text
        sender.idGrupo = user.id_grupo
        sender.idUsuario = user.id

        fetch(config.endpoints.chat, {
            method: 'put',
            body: JSON.stringify(sender)
        })
        .then(res => res.json())
        .then(json => {
            if (!json.success){
                swal({
                    title: 'Erro',
                    text: json.mensagem,
                    icon: 'error'
                })
            }else{
                values.mensagem = ''
            }
        })
    }
})(SendMessage)

export default FormikApp