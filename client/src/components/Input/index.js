import React, { Fragment } from 'react'
import Label from '../Label'
import './style.css';

const Input = ({label, placeholder, type}) => {
    return (
        <Fragment>
            <Label text={label} />
            <input className="input" type={type} placeholder={placeholder} />
        </Fragment>
    )
}

export default Input