import React from 'react'
import './style.css'

const Label = ({text}) => {
    return (
        <label className="label">{text}</label>
    )
}

export default Label