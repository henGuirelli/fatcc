import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import { routers } from '../../config.json'

import Icon from '../../images/sys_images/menu.svg'
import imgGroup from '../../images/sys_images/group.svg'
import imgHome from '../../images/sys_images/home.svg'
import imgLogoff from '../../images/sys_images/logoff.svg'
import imgTeacher from '../../images/sys_images/teacher.svg'
import imgProfile from '../../images/sys_images/profile.svg'

import './style.css'
import ProfilePic from '../ProfilePic'

const labelHome = 'Home'
const labelGroup = 'Meu Grupo'
const labelLogoff = 'Logoff'
const labelTeacher = 'Orientador'
const labelProfile = 'Perfil'

class MenuIcon extends Component {
    constructor(){
        super()
        this.state = {
            classMenu: 'display-hidden',
            nome: '' //nome da pessoa logada
        }
    }

    ToggleMenu(){
        if (this.state.classMenu === 'display-hidden')
            this.setState({
                classMenu: 'display-block'
            })
        else
            this.setState({
                classMenu: 'display-hidden'
            })
    }

    render () {
        const { classMenu, nome } = this.state
        return (
            <Fragment>
                <div className={ 'container-menu ' + classMenu } onClick={ this.ToggleMenu.bind(this) }>
                    <div className='menu ' >
                        <div className='content-menu'>
                            <div className='profile-pic-container'>
                                <ProfilePic />
                                <h2>{ nome }</h2>
                            </div>
                            <nav>
                                <ul>
                                    <li className='item-menu'>
                                        <Link className='link-router' to={routers.home}>
                                            <div className='container-icon-menu'>
                                                <img src={imgHome} alt='icone do menu' />
                                            </div>
                                            <span className='text'>
                                                { labelHome }
                                            </span>
                                        </Link>
                                    </li>
                                    <li className='item-menu'>
                                        <Link className='link-router' to={routers.grupo}>
                                            <div className='container-icon-menu'>
                                                <img src={imgGroup} alt='icone do menu' />
                                            </div>
                                            <span className='text'>
                                                { labelGroup }
                                            </span>
                                        </Link>
                                    </li>
                                    <li className='item-menu'>
                                        <Link className='link-router' to={routers.orientador}>
                                            <div className='container-icon-menu'>
                                                <img src={imgTeacher} alt='icone do menu' />
                                            </div>
                                            <span className='text'>
                                                { labelTeacher }
                                            </span>
                                        </Link>
                                    </li>
                                    <li className='item-menu'>
                                        <Link className='link-router' to={routers.perfil}>
                                            <div className='container-icon-menu'>
                                                <img src={imgProfile} alt='icone do menu' />
                                            </div>
                                            <span className='text'>
                                                { labelProfile }
                                            </span>
                                        </Link>
                                    </li>
                                    <li className='item-menu'>
                                        <Link className='link-router' to={routers.login}>
                                            <div className='container-icon-menu'>
                                                <img src={imgLogoff} alt='icone do menu' />
                                            </div>
                                            <span className='text'>
                                                { labelLogoff }
                                            </span>
                                        </Link>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div className="container-menu-icon">
                    <div className="menu-icon" onClick={ this.ToggleMenu.bind(this) }>
                        <img src={Icon} alt="icone do menu" />
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default MenuIcon