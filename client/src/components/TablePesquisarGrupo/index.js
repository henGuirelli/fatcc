import React from 'react'
import './style.css'

const TablePesquisarGrupo = (children) => {
    return (
        <table className="table-pesquisar-grupo">
            <thead className="head">
                <tr>
                    <th>Tema</th>
                    <th>Criador</th>
                </tr>
            </thead>
            <tbody className="body">
                {
                    children.map(grupo => <tr> <td> { grupo.nome } </td><td> { grupo.criador } </td></tr>)
                }    
            </tbody>

        </table>
    )
}

export default TablePesquisarGrupo