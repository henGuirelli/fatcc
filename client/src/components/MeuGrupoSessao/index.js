import React from 'react'
import Label from '../Label'
import './style.css'
import edit from '../../images/sys_images/edit.svg'

const SessaoMeuGrupo = ({title, text}) => {
    return (
        <section className="section-meu-grupo">
            <div className="title-section">
                <div className="inline">
                    <Label text={title} />
                </div>
                <div className="inline image-wrapper">
                    <img src={edit} alt="botão editar"/>
                </div>
            </div>
            <hr/>
            <p className="text">{text}</p>
        </section>
    )
}

export default SessaoMeuGrupo