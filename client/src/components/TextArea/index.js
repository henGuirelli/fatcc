import React, { Fragment } from 'react'
import Label from '../Label'
import './style.css'

const TextArea = ({content, label, rows = 4, cols = 30}) => {
    return (
        <Fragment>
            {label &&  <Label text={label} />}
            <textarea rows={rows} cols={cols} className="text-area">
                {content}
            </textarea>
        </Fragment>
    )
}

export default TextArea