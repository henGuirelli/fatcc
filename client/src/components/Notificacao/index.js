import React, { Fragment } from 'react'
import './style.css'

const Notificacao = (props, { nome, login, mensagem, tipoNotificacao }) => {
    console.log(nome, login)
    return (
        <div className='notificacao' {...props}>
            <p> { props.mensagem } </p>   
            <label className='button btn-notificacao'>Recusar</label> 
            <label className='button btn-notificacao'>Aceitar</label> 
        </div>
    )
}

export default Notificacao