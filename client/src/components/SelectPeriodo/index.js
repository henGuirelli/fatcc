import React, { Fragment } from 'react'
import './style.css'
import Label from '../Label'

const Select = ({label}) => {
    return (
        <Fragment>
            <Label text={label} />
            <select className="select">
                <option value="diurno">Diurno</option>
                <option value="vespertino">Vespertino</option>
                <option value="noturno">Noturno</option>
            </select>
        </Fragment>
    )
}

export default Select