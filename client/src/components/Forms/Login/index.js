import React from 'react'
import { withFormik, Form } from 'formik'
import * as Yup from 'yup'
import Input from '../Input'
import Button from '../../Button'
import ErrorMessage from '../Error'
import config from '../../../config.json'
import './style.css'
import swal from 'sweetalert'

const emailRequiredError = 'E-mail é um campo obrigatório'
const emailError = 'E-mail inválido'
const senhaMinError = 'Senha deve conter no minimo 6 caracteres'
const senhaRequeridError = 'Senha é obrigatória'

const minTamSenha = 6

const emailLabel = 'E-mail'
const senhaLabel = 'Senha'
const senhaPlaceHolder = 'Minimo 6 caracteres'
const emailPlaceHolder = 'E-mail institucional'

const FormLogin = ({ values, handleChange, handleSubmit, errors, touched }) => (
    <Form method='post'>
        <div className='input-wrapper-login'>
            <Input name='email' label={emailLabel} placeholder={emailPlaceHolder} />
            {touched.email && errors.email && <ErrorMessage>{ errors.email }</ErrorMessage>}
        </div>

        <div className='input-wrapper-login'>
            <Input name='senha' type='password' label={senhaLabel} placeholder={senhaPlaceHolder} /> 
            {touched.senha && errors.senha && <ErrorMessage>{ errors.senha }</ErrorMessage>}
        </div>

        <Button type='submit' text='Entrar' />
    </Form>
)

const FormikApp = withFormik({
    mapPropsToValues: (props) => ({
        email: '',
        senha: ''
    }),

    validationSchema: Yup.object().shape({
        email: Yup.string().email(emailError).required(emailRequiredError),
        senha: Yup.string().min(minTamSenha, senhaMinError).required(senhaRequeridError)
    }),

    handleSubmit(values){
        console.log('values handle submit')
        console.log(values)
        fetch(config.endpoints.login, {
            method: 'post',
            body: JSON.stringify(values)
        })
        .then(result => result.json())
        .then(json => {
            if (json.loginValid === false)
                swal({
                    title: 'Erro',
                    text: 'Senha ou login inválidos',
                    icon: 'error'
                })
            else{
                sessionStorage.setItem('user', JSON.stringify(json.user))
                window.location = config.routers.home
            }
        })
    }
})(FormLogin)

export default FormikApp
