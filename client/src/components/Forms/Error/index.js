import React from 'react'
import './style.css'

const ErrorMessage = ({ children }) => (
    <div className='error'>
        <span className='mensagem'> { children } </span>
    </div>
)

export default ErrorMessage