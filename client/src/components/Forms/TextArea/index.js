import React from 'react'
import { Field } from 'formik'
import './style.css'

const TextArea = props => {
    const { label } = props
   
    return (
        <label className='label'>
            { label && <span> {label} </span> }
            <Field className='text-area' rows='7' { ...props } component='textarea' />
        </label>
    )
}

export default TextArea