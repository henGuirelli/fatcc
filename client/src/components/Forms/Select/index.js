import React from 'react'
import { Field } from 'formik'
import './style.css'


const Select = ({ label, name, options }) => (
    <label className='label'>
        <span> {label} </span>
        <Field name={name} component='select' className='input' >
            <option>Selecione...</option>
            { options.map(item => <option key={item}> {item} </option>) }
        </Field>
    </label>
)

export default Select