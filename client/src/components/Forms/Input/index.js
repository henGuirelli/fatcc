import React, { Fragment } from 'react'
import './style.css'
import { Field } from 'formik'

const Input = props => {
    return <Fragment><label > { props.label } </label><Field { ...props } className='input' /></Fragment>
}

export default Input