import React from 'react'
import { withFormik, Form } from 'formik'
import * as Yup from 'yup'
import Input from '../Input'
import TextArea from '../TextArea'
import Button from '../../Button'
import ErrorMessage from '../Error'
import config from '../../../config.json'
import './style.css'
import swal from 'sweetalert'
import queryString from 'query-string'

const labelNome = 'Nome'
const labelDescricao = 'Descrição'

const placeholderNome = ''
const placeholderDescrição = 'Limite de 150 caracteres'

const maxCaracteresDescricao = 150

const nomeRequiredError = 'Titulo do TCC é obrigatório'
const descricaoRequiredError = 'Descrição é obrigatório'
const descricaoMaxerror = 'Limite de '+ maxCaracteresDescricao +' caracteres'

const FormLogin = ({ isSubmitting, errors, touched }) => (
    <Form>
        <div className='input-wrapper-login'>
            <Input name='nome' label={labelNome} placeholder={placeholderNome} />
            {touched.nome && errors.nome && <ErrorMessage>{ errors.nome }</ErrorMessage>}
        </div>

        <div className='input-wrapper-login'>
            <TextArea name='descricao' label={labelDescricao} placeholder={placeholderDescrição} /> 
            {touched.descricao && errors.descricao && <ErrorMessage>{ errors.descricao }</ErrorMessage>}
        </div>

        <Button disabled={isSubmitting} type='submit' text={isSubmitting ? 'Aguarde' : 'Criar' } />
    </Form>
)

const FormikApp = withFormik({
    mapPropsToValues: props => ({
        nome: '',
        descricao: ''
    }),

    validationSchema: Yup.object().shape({
        nome: Yup.string().required(nomeRequiredError),
        descricao: Yup.string().max(maxCaracteresDescricao, descricaoMaxerror).required(descricaoRequiredError)
    }),

    handleSubmit(values, { setSubmitting }){
        const query = `${config.endpoints.criarGrupo}?${queryString.stringify(values)}`
        values['user'] = JSON.parse(sessionStorage.getItem('user'))

        fetch(config.endpoints.criarGrupo, {
            method: 'put',
            body: JSON.stringify(values)
        })
        .then(result => result.json())
        .then(json => {
            setSubmitting(false)
            if (json.success){
                swal({
                    title: 'Sucesso',
                    text: 'Grupo cadastrado',
                    icon: 'success'
                })
                .then((value) => {
                    let user = JSON.parse(sessionStorage.getItem('user'))

                    fetch(config.endpoints.atualizarUsuario + user.id, {
                        method: 'get'
                    })
                    .then(result => result.json())
                    .then(json => {
                        sessionStorage.setItem('user', JSON.stringify(json.user))
                        window.location = config.routers.grupo
                    })
                })
            }
            else
            {
                swal({
                    title: 'Erro',
                    text: json.error,
                    icon: 'error'
                })
            }
        })
    }
})(FormLogin)

export default FormikApp
