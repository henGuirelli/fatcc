import React from 'react'
import { withFormik, Form } from 'formik'
import * as Yup from 'yup'
import Input from '../Input'
import Button from '../../Button'
import ErrorMessage from '../Error'
import config from '../../../config.json'
import swal from 'sweetalert'
import './style.css'

const emailRequiredError = 'E-mail é um campo obrigatório'
const emailError = 'E-mail inválido'
const emailInstitucionalError = 'Deve ser um email institucional'
const senhaMinError = 'Senha deve conter no minimo 6 caracteres'
const senhaRequeridError = 'Senha é obrigatória'
const confirmarsenhaRequirdError = 'Confirmar senha é obrigatório'
const confirmarSenhaDiferentesError = 'As senhas não são iguais'

const minTamSenha = 6

const emailLabel = 'E-mail'
const senhaLabel = 'Senha'
const confirmarSenhaLabel = 'Confirmar Senha'
const confirmarSenhaPlaceHolder = 'Minimo 6 caracteres'
const senhaPlaceHolder = 'Minimo 6 caracteres'
const emailPlaceHolder = 'E-mail institucional'

const FormCadastro = ({ values, handleChange, handleSubmit, errors, touched }) => (
    <Form>
        <div className='input-wrapper-cadastro'>
            <Input name='email' label={emailLabel} placeholder={emailPlaceHolder} />
            {touched.email && errors.email && <ErrorMessage>{ errors.email }</ErrorMessage>}
        </div>

        <div className='input-wrapper-cadastro'>
            <Input name='senha' type='password' label={senhaLabel} placeholder={senhaPlaceHolder} /> 
            {touched.senha && errors.senha && <ErrorMessage>{ errors.senha }</ErrorMessage>}
        </div>

        <div className='input-wrapper-cadastro'>
            <Input name='confirmarSenha' type='password' label={confirmarSenhaLabel} placeholder={confirmarSenhaPlaceHolder} /> 
            {touched.confirmarSenha && errors.confirmarSenha && <ErrorMessage>{ errors.confirmarSenha }</ErrorMessage>}
        </div>

        <Button type='submit' text='Cadastrar' />
    </Form>
)

const FormikApp = withFormik({
    mapPropsToValues: (props) => ({
        email: '',
        senha: '',
        confirmarSenha: ''
    }),

    validationSchema: Yup.object().shape({
        email: Yup.string().email(emailError).matches(/@fatec.sp.gov.br/, emailInstitucionalError).required(emailRequiredError),
        senha: Yup.string().min(minTamSenha, senhaMinError).required(senhaRequeridError),
        confirmarSenha: Yup.string()
        .oneOf([Yup.ref('senha'), null], confirmarSenhaDiferentesError)
        .required(confirmarsenhaRequirdError)
    }),

    handleSubmit(values){
        fetch(config.endpoints.login, {
            method: 'put',
            body: JSON.stringify(values)
        })
        .then(res => res.json())
        .then(json => {
            if (json.success){
                swal({
                    title: 'Sucesso',
                    text: json.mensagem,
                    icon: 'success'
                }).then(() => {
                    window.location = '/login'
                })
            }else{
                swal({
                    title: 'Erro',
                    text: json.mensagem,
                    icon: 'error'
                })
            }
        })
    }
})(FormCadastro)

export default FormikApp
