import React from 'react'
import { withFormik, Form } from 'formik'
import * as Yup from 'yup'
import Input from '../Input'
import Button from '../../Button'
import ErrorMessage from '../Error'
import './style.css'
import ProfilePic from '../../ProfilePic'
import defaultProfilePic from '../../../images/profile/default-user-profile.jpg'
import Select from '../Select'
import swal from 'sweetalert'

const labelNome = 'Nome'
const labelCurso = 'Curso'
const labelPeriodo = 'Periodo'
const labelEmail = 'Email'
const labelAlterarImagem = 'Alterar'

const emailRequiredError = 'E-mail é um campo obrigatório'
const emailError = 'E-mail inválido'

const optionPeriodo = ['Matutino', 'Vespertino', 'Diurno']
const optionCurso = ['ADS', 'SEG', 'JOGOS']

const FormPerfil = ({ errors, touched }) => (
    <Form>
        <div className='profile-pic-wrapper'>
            <ProfilePic src={defaultProfilePic} />
        </div>

        <div className='button-profile-pic'>
            <input className='escolher-arquivo' id='escolher-arquivo' type='file' />
            <label className='button' for='escolher-arquivo'>{labelAlterarImagem}</label>
        </div>

        <div className='input-wrapper-cadastro'>
            <Input name='nome' label={labelNome} />
        </div>

        <div className='input-wrapper-cadastro'>
            <Input name='email' label={labelEmail} />
            {touched.email && errors.email && <ErrorMessage>{ errors.email }</ErrorMessage>}
        </div>

        <div className='input-wrapper-cadastro'>
            <Select name='curso' label={labelCurso} options={optionCurso} />
        </div>

        <div className='input-wrapper-cadastro'>
            <Select name='periodo' type='password' label={labelPeriodo} options={optionPeriodo} />
        </div>

        <Button text='Salvar' />
    </Form>
)

const FormikApp = withFormik({
    mapPropsToValues: ({ nome, curso, periodo, email }) => ({
        nome: nome || '',
        email: email || '',
        curso: curso || '',
        periodo: periodo || ''
    }),

    validationSchema: Yup.object().shape({
        email: Yup.string().email(emailError).required(emailRequiredError)        
    }),

    handleSubmit(values){
        const user = JSON.parse(sessionStorage.getItem('user'))
        const url = "http://localhost:8085/perfil/" + user.id

        fetch (url, {
            method: 'put',
            body: JSON.stringify({
                values
            })
        })
        .then(result => result.json())
        .then(result => {
            if (result.success){
                swal({
                    title: 'Sucesso',
                    text: 'Perfil editado',
                    icon: 'success'
                })
            }else{
                swal({
                    title: 'erro',
                    text: 'erro ao salvar\n' + result.error,
                    icon: 'error'
                })
            }
        })
    }
})(FormPerfil)

export default FormikApp
