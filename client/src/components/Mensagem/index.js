import React from 'react'
import defaultImg from '../../images/profile/default-user-profile.jpg'
import './style.css'
import ProfilePic from '../ProfilePic';

const Mensagem = ({profilePic = defaultImg, sender, children}) => {
    return (
        <div className="mensagem">
            <div className="imagem-wrapper">
                <ProfilePic src={profilePic} />
            </div>
            <div className="inline wrapper">
                <div className="sender">
                    <span>{sender}</span>
                </div>

                <div className="content">
                    <p>{children}</p>
                </div>
            </div>
        </div>
    )
}

export default Mensagem