import React from 'react'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      marginBottom: 10,
      overflowX: 'auto',
    },
    table: {
      minWidth: 300,
    },
  });

const TableIntegrantes = props => {
    const {integrantes, classes} = props
    return (
        <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell> Nome </TableCell>
              <TableCell> Remover </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {integrantes.map(integrante => {
              return (
                <TableRow key={integrante.id}>
                  <TableCell> { integrante.nome || integrante.login } </TableCell>
                  <TableCell component="button">X</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    )
}

export default withStyles(styles)(TableIntegrantes);