import React from 'react'
import Input from '../../components/Input'
import TableOrientador from '../../components/TableOrientador'
import MenuIcon from '../../components/MenuIcon'
import config from '../../config.json'

class Orientador  extends React.Component {
    state = {orientadores: [{nome: 'carregando...', especialidades: []}]}

    constructor(props){
        super(props)        
        this.carregarOrientador()
        this.carregarOrientadorAsync()
    }

    carregarOrientadorAsync(){
        setInterval(() => {
            this.carregarOrientador()
        }, 60000);        
    }

    carregarOrientador(){
        fetch("http://localhost:8085/orientadores", {
            method: 'get'
        })
        .then(result => 
            result.json())
        .then(result => {
            if (result.success)
                this.setState({orientadores: result.orientadores})
        })
    }

    render(){

        return (
            <div className='container'>
                <MenuIcon />
                <TableOrientador orientadores={this.state.orientadores}/>
            </div>
        )
    }
}

export default Orientador