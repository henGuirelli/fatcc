import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './style.css'
import Titulo from '../../components/Titulo'
import FormikApp from '../../components/Forms/Login'
import Button from '../../components/Button'

class Login extends Component {
    
    render(){
        return (
            <div className='container'>
                <center>
                    <Titulo text='FaTCC' />
                </center>
                <FormikApp />
                <Link to='/cadastro'>
                    <Button type='button' text='Cadastrar' />
                </Link>
            </div>
        )
    }
}

export default Login