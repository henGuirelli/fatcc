import React from 'react'
import MenuIcon from '../../components/MenuIcon'
import Button from '../../components/Button'
import './style.css'
import { Link } from 'react-router-dom'

const SemGrupo = () => {
    return (
        <div className="container">
            <MenuIcon />
            <div className="main">
                <span className="text-span"> Você ainda não possui um grupo :( </span>
                <span className="text-span centralize-span"> Você pode: </span>
                <Link to='grupo/criar'>
                    <Button text="Criar Novo Grupo"/>
                </Link>
                <span className="text-span centralize-span"> Ou </span>
                <Button text="Buscar Grupo"/>
            </div>
        </div>
    )
}

export default SemGrupo