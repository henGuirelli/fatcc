import React from 'react'
import Input from '../../components/Input'
import MenuIcon from '../../components/MenuIcon'
import TablePesquisarGrupo from '../../components/TablePesquisarGrupo';
import Button from '../../components/Button';

class BuscarGrupo extends React.Component {

    
    render(){
        return (
            <div className="container">
                <MenuIcon />
                <Input label="Pesquisar:" />
                <br/><br/>
                <TablePesquisarGrupo />
                <Button text="Selecionar"/>
            </div>
        )
    }
}

export default BuscarGrupo