import React from 'react'
import './style.css'
import MenuIcon from '../../components/MenuIcon'
import FormikApp from '../../components/Forms/Perfil';

const Perfil = () => {
    return (
        <div className="container">
            <MenuIcon />
            <FormikApp />
        </div>
    )
}

export default Perfil