import React from 'react'
import MenuIcon from '../../components/MenuIcon'
import Item from '../../components/MeuGrupoSessao'
import Titulo from '../../components/Titulo'
import Label from '../../components/Label'
import Button from '../../components/Button'
import './style.css'
import TableIntegrantes from '../../components/TableIntegrantes'

import config from '../../config.json'

const loading = {
    grupo: {
        nome: 'Carregando...',
        orientador: 'Carregando...',
        descricao: 'Carregando...'
    },
    integrantes: []
}

class MeuGrupo extends React.Component {
    state = loading

    componentDidMount(){
        let user = JSON.parse(sessionStorage.getItem('user'))
        fetch(config.endpoints.buscarGrupo.replace(':id', user.id_grupo))
        .then(result => result.json())
        .then(json => {
            this.setState(json)
        })
    }

    handleSairDoGrupo = () => {
        let user = JSON.parse(sessionStorage.getItem('user'))
        fetch(config.endpoints.sairGrupo.replace(':id_usuario', user.id), {
            method: 'put'
        }).then(result => result.json())
        .then(json => {
            delete user.id_grupo
            sessionStorage.setItem('user', JSON.stringify(user))
            window.location = config.routers.grupo
        })
    }

    render (){
        let { grupo, integrantes } = this.state
        return (
            <div className="container">
                <MenuIcon />
                <Titulo text="Opções" />
                <Item title="Grupo" text={grupo.nome} />
                <Item title="Orientador" text={grupo.orientador || 'Sem orientador'} />
                <Item title="Descrição do tema" text={grupo.descricao || 'Sem descrição'} />
                <Label text="Integrantes:"/>
                <TableIntegrantes integrantes={integrantes}/>
                <Button onClick={this.handleSairDoGrupo} text="Sair do Grupo"/>
            </div>
        )
    }
}

export default MeuGrupo