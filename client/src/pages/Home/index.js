import React, { Fragment } from 'react'
import Titulo from '../../components/Titulo'
import MenuIcon from '../../components/MenuIcon'
import Notificacao from '../../components/Notificacao'

class Home extends React.Component {
    state = {notificacoes: []}

    constructor(props){
        super(props)
        if (JSON.parse(sessionStorage.getItem('user')) == null || JSON.parse(sessionStorage.getItem('user')) == undefined){
            window.location = '\login'
        }
        this.carregaNotificacoes();
        this.carregaNotificacoesAsync();
    }

    carregaNotificacoes(){
        let user = JSON.parse(sessionStorage.getItem('user')).id
        console.log("http://localhost:8085/notificacoes/" + user)
        fetch("http://localhost:8085/notificacoes/" + user, {
            method: 'get'
        })
        .then(result => result.json())
        .then(result => {
            console.log(result)
            if (result.success)
                this.setState({notificacoes: result.notificacoes})
        })
    }

    carregaNotificacoesAsync(){
        setInterval(() => {
            this.carregaNotificacoes()
        }, 10000);
    }

    render(){
        
        let isOrientador = JSON.parse(sessionStorage.getItem('user')).tipoConta == 2
        return (            
            <div className='container'>
                <MenuIcon />
                <Titulo text='Home' />
                <h2> Notificações: </h2>
                <hr />
                {
                    isOrientador ?
                    this.state.notificacoes.map((item, index) => 
                        <Fragment>
                            <Notificacao 
                                key={index} 
                                nome={item.de}
                                login={item.login}
                                mensagem={item.mensagem}
                                tipoNotificacao={item.tipoNotificacao}
                            />
                            <hr />
                        </Fragment>
                    )
                        : null
                }
            </div>
        )
    }
}

export default Home