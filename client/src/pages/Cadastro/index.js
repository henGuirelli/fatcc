import React from 'react'
import './style.css'
import Titulo from '../../components/Titulo'
import FormikApp from '../../components/Forms/Cadastro'

const Cadastro = () => {
    return (
        <div className="container">
            <Titulo text="Cadastro" />
            <FormikApp />
        </div>
    )
}

export default Cadastro