import React from 'react'
import Mensagem from '../../components/Mensagem'
import MenuIcon from '../../components/MenuIcon'
import SendMessage from '../../components/SendMessage'
import Titulo from '../../components/Titulo'
import './style.css'

import config from '../../config.json'

const loading = {
    mensagens: []
}

class Chat extends React.Component {
    state = loading
    
    update(){
        let user = JSON.parse(sessionStorage.getItem('user'))
        fetch(config.endpoints.chat + user.id_grupo)
        .then(result => result.json())
        .then(json => {
            this.setState(json)
            try{
                let textarea = document.getElementsByClassName('chat')[0]
                textarea.scrollTop = textarea.scrollHeight;
            }catch (ex) {}
        })
    }

    componentDidMount(){
        this.update()
        setInterval(() => {
            this.update()
        }, 1000)
    }

    render(){
        const { mensagens } = this.state
        return (
            <div className='container'>
                <MenuIcon />
                <Titulo text='Chat' />
                <div className='chat'>
                    { mensagens.map(mensagem => (
                        <Mensagem sender={ mensagem.nome || mensagem.login }>{ mensagem.mensagem }</Mensagem>
                    )) }
                </div>

                <SendMessage update={ this.update }/>
            </div>
        )
    }
}

export default Chat