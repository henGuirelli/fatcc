import React from 'react'
import Titulo from '../../components/Titulo'
import MenuIcon from '../../components/MenuIcon'
import FormikApp from '../../components/Forms/CriarGrupo'

import './style.css';

const CriarGrupo = () => {
    return (
        <div className="container">
            <MenuIcon />
            <Titulo text="Criar Grupo" />
            <FormikApp />
        </div>
    )
}

export default CriarGrupo