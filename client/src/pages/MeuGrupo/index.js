import React from 'react'
import Button from '../../components/Button'
import MenuIcon from '../../components/MenuIcon'
import './style.css'
import Titulo from '../../components/Titulo'
import SemGrupo from '../SemGrupo'

import LinkButton from '../../components/LinkButton'

import config from '../../config.json'

const MeuGrupo = () => {
    let user = JSON.parse(sessionStorage.getItem('user'))
    if (user.id_grupo !== undefined && user.id_grupo !== null)
        return (
            <div className="container">
                <MenuIcon />
                <Titulo text="Meu Grupo"/>
                <div className="centralize-vertical">
                    <LinkButton to={config.routers.chat} text="Chat"/>
                    <LinkButton to={config.routers.opcoesGrupo} text="Opções"/>
                </div>
            </div>
        )
    else
        return <SemGrupo />
}

export default MeuGrupo