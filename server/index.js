const restify = require('restify');
const server = restify.createServer();
const corsMiddleWare = require('restify-cors-middleware')
const mysql = require('mysql')

const cors = corsMiddleWare({
    origins: ['*']
})

const connection = mysql.createConnection({
	host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'fatcc'
})

server.use(restify.plugins.bodyParser())
server.pre(cors.preflight) 
server.use(cors.actual)
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

/* editar perfil */
server.put('/perfil/:id_usuario', (req, res, next) => {
	const  id  = req.params.id_usuario
	const { nome, email, curso, periodo } = JSON.parse(req.body).values
	const sql = `UPDATE usuarios SET nome = '${nome}', login = '${email}'
				 WHERE id = ${id}`
	console.log(sql)
	connection.query(sql, (error, results, fields) => {
		if (error){
			res.send({ success: false, error})
		}else{
			res.send({success: true})
		}
	})
})


/* logar */
server.post('/login', (req, res, next) => {
	const { email, senha } = JSON.parse(req.body)
	const query = `
	SELECT * FROM 
		usuarios 
	WHERE 
		login = '${email}' AND senha = '${senha}' 
	UNION ALL
	SELECT 
		p.id, p.nome, null as emailVerificado, 2 as tipoConta, null as imagem, 0 as criador, 
		null as id_grupo, p.login, p.senha from professor p where login = '${email}' and senha = '${senha}'`
 
	connection.query(query, (error, results, fields) => {
		if (error) throw error;
		if (results.length === 0){
			res.send({
				loginValid: false,
				user: {}
			})
		}else{
			let resp =  results[0]
			delete resp.senha
			res.send({
				loginValid: true,
				user: resp
			})
		}
	});
})

/* criar conta */
server.put('/login', (req, res, next) => {
	const { email, senha } = JSON.parse(req.body)
	let query = `SELECT login FROM usuarios WHERE login = '${email}' and senha = '${senha}'`
	connection.query(query, (error, results, fields) => {
		if (error) throw error;
		if (results.length > 0){
			res.send({
				success: false,
				mensagem: "E-mail já cadastrado"
			})
			return
		}
		else
		{
			query = `INSERT INTO usuarios(tipoConta, login, senha) VALUES (1, '${email}', '${senha}')`
			connection.query(query, (error, results, fields) => {
				if (error) throw error;
				res.send({
					success: true,
					mensagem: "E-mail cadastrado com sucesso"
				})
			})
		}
	})
})

/* criar grupo */
server.put('/grupo/criar', (req, res, next) => {
	const { nome, descricao, user } = JSON.parse(req.body)
	let query = `INSERT INTO grupos(nome, descricao) VALUES ('${nome}', '${descricao}')`
	connection.query(query, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error
			})
			throw error 
		}
		query = `UPDATE usuarios SET id_grupo = (SELECT id FROM grupos ORDER BY id DESC LIMIT 1), criador = true WHERE id = ${user.id}`
		connection.query(query, (error, results, fields) => {
			if (error){
				res.send({
					success: false,
					error
				})
				throw error 
			}
			res.send({
				success: true
			})
		})
	})
})

/* sair do grupo */
server.put('/grupo/sair/:id_usuario', (req, res, next) => {
	const idUsuario = req.params.id_usuario
	const query = `UPDATE usuarios SET id_grupo = null WHERE id = ${idUsuario}`

	connection.query(query, (error, results, field) => {
		if (error) {
			res.send({
				success: false,
				error
			})
		}else{
			res.send({
				success: true
			})
		}

	})
})

server.get('/select/:id', (req, res, next) => {
	const { id } = req.params
	const query = `SELECT * FROM usuarios WHERE id = ${id}`
	connection.query(query, (error, results, field) => {
		if (error) throw error
		
		const user = results[0]
		
		if (user === undefined)
			res.send({
				success: false
			})
		else
			res.send({
				success: true,
				user
			})
	})
})

server.get('/grupos/:id', (req, res, next) => {
	const { id } = req.params
	console.log(req.params)
	let query = `SELECT * FROM grupos WHERE id = ${id} LIMIT 1`

	let response = {}

	connection.query(query, (error, results, field) => {
		if (error) res.send({success: false, error})
		
		response['grupo'] = results[0] || {}
		
		query = `SELECT id, nome, login FROM usuarios WHERE id_grupo = ${id}`
		connection.query(query, (error, results, field) => {
			if (error) res.send({success: false, error})

			response['integrantes'] = results
			response['success'] = true

			res.send(response)
		})
	})
})

server.get('/chat/:id_grupo', (req, res, next) => {
	const idGrupo = req.params.id_grupo
	const query = `SELECT c.mensagem, u.nome, u.login FROM chat c INNER JOIN usuarios u ON c.id_usuario = u.id WHERE c.id_grupo = ${idGrupo}`


	connection.query(query, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error
			})
		}else{
			res.send({
				success: true,
				mensagens: results
			})
		}
	})
})

server.put('/chat/', (req, res, next) => {
	console.log(req.body) 
	const { mensagem, idGrupo, idUsuario } = JSON.parse(req.body)
	const query = `INSERT INTO chat(mensagem, id_grupo, id_usuario) VALUES('${mensagem}', ${idGrupo}, ${idUsuario})`
	connection.query(query, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error
			})
			throw error
		}else{
			res.send({
				success: true
			})
		}
	})
})

server.get('/orientadores', (req, res, next) => {
	const query = `SELECT * FROM professor`
	
	connection.query(query, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error
			})
			throw error
		}else{
			res.send({
				success: true,
				orientadores: results
			})
		}
	})
})

server.put('/grupos/solicitarOrientador/:id_usuario', (req, res, next) => { 
	// id_usuario = quem irá receber da notificação
	const idGrupo = req.body
	const id_usuario = req.params.id_usuario
	let sql = `SELECT nome from grupos where id = ${req.body}`
	connection.query(sql, (error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error
			})
		}else{
			const nome = results[0].nome
			sql = `INSERT INTO notificacoes(mensagem, de, para) VALUES (
				'O Grupo ${nome} quer sua orientação', ${idGrupo}, ${id_usuario}
			)`
			connection.query(sql, (error, results, fields) => {
				if (error){
					res.send({
						success: false,
						error
					})
					throw error;
				}else{
					console.log('sucesso')
					res.send({
						success: true
					})
				}
			})
		}
	})

	
})

server.get('/notificacoes/:id_usuario', (req, res, next) => {
	const sql = `SELECT * FROM notificacoes WHERE para = ${req.params.id_usuario}`

	connection.query(sql,(error, results, fields) => {
		if (error){
			res.send({
				success: false,
				error
			})
		}else{
			res.send({
				success: true,
				notificacoes: results
			})
		}
	})
})

server.listen(8085, () => {
	console.log('%s listening at %s', server.name, server.url);
}); 