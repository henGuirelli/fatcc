drop database fatcc;
create database if not exists fatcc;
use fatcc;

create table if not exists grupos(
	id int auto_increment primary key,
	nome varchar(40),
    descricao text,
    imagem varchar(65)
);

create table if not exists usuarios (
	id int primary key auto_increment not null,
    nome varchar(30),
    emailVerificado bool default false,
    /*id_tipoConta int not null,*/
    tipoConta int,
    imagem varchar(150),
    criador boolean default false,
    id_grupo int,
    login varchar(50) not null,
    senha varchar(50) not null,
    /*foreign key (id_tipoConta) references tipoConta(id),*/
    foreign key (id_grupo) references grupos(id)    
);

create table if not exists chat (
	mensagem text,
    id_usuario int,
    id_grupo int,
    foreign key (id_usuario) references usuarios(id),
    foreign key (id_grupo) references grupos(id)
);

create table if not exists professor (
	id int auto_increment primary key,
    nome varchar(30) not null,
    login varchar(30) not null,
    senha varchar(30) not null
);

insert into professor(nome, login, senha) values 
('Enzo', 'enzo@fatec.sp.gov.br', 'senha123'), 
('Robson', 'robson@fatec.sp.gov.br', 'senha123');

create table if not exists notificacoes (
    mensagem varchar(300),
    de int,
    para int
);
